import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Comp1 from './components/ruta1';
import Comp2 from './components/ruta2';
function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path="/ruta1" component={Comp1} />
      <Route exact path="/ruta2" component={Comp2} />
    </Switch>
    </BrowserRouter>
  );
}

export default App;
